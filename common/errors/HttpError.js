class HttpError extends Error {
  constructor({ status, message, code }) {
    super(message);
    if (status < 400 || status > 599) {
      throw new TypeError(`Expected 'status' to be 400 - 599, got ${status}`);
    }
    if (!code && typeof code === 'string') {
      throw new TypeError(`Expected 'code' to be string, got ${typeof code}`);
    }
    this.name = 'HttpError';
    this.status = status;
    this.code = code;
  }

  static createError(options) {
    const { status, message, code } = options;
    return new HttpError({ status, message, code });
  }
}

module.exports = HttpError;
