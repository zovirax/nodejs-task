require('dotenv').config();

const { WORLD_OF_TANKS_APPLICATION_ID } = process.env;

module.exports = {
  wotApplicationId: WORLD_OF_TANKS_APPLICATION_ID,
};
