require('dotenv').config();

const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_CLIENT } = process.env;

module.exports = {
  database: DB_NAME,
  user: DB_USERNAME,
  password: DB_PASSWORD,
  host: DB_HOST,
  client: DB_CLIENT,
};
