const { verifyJwt, createDummyJwt } = require('../../common/helpers/jwt.helper');
const { secretKey } = require('../jwt.config');

const jwtFromRequest = request => {
  // accessToken can be provided either in `Authorization` header or `accessToken` cookie'
  const { authorization } = request.headers;
  const { accessToken } = request.cookies;
  if (!(authorization || accessToken)) {
    return createDummyJwt();
  }
  const jwt = authorization ? authorization.split('Bearer ')[1] : accessToken;
  try {
    verifyJwt(jwt);
    return jwt;
  } catch (err) {
    console.error(err);
    return createDummyJwt();
  }
};

module.exports = {
  jwtOptions: {
    jwtFromRequest,
    secretOrKey: secretKey,
  },
};
