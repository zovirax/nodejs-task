const Role = Object.freeze({
  ALL: 'ALL',
  ANONYMOUS: 'ANONYMOUS',
  AUTHORIZED: 'AUTHORIZED',
});

const routes = [
  {
    resource: '/api/auth/logout',
    permissions: [
      { method: 'POST', roles: [Role.AUTHORIZED] },
    ],
  },
  {
    resource: '/api/auth/**',
    permissions: [
      { method: 'GET', roles: [Role.ALL] },
      { method: 'POST', roles: [Role.ALL] },
    ],
  },
  {
    resource: '/api/user/**',
    permissions: [
      { method: 'PATCH', roles: [Role.AUTHORIZED] },
      { method: 'PUT', roles: [Role.AUTHORIZED] },
    ],
  },
  {
    resource: '/api/game/**',
    permissions: [
      { method: 'GET', roles: [Role.AUTHORIZED] },
      { method: 'POST', roles: [Role.AUTHORIZED] },
      { method: 'DELETE', roles: [Role.AUTHORIZED] },
    ],
  },
];

module.exports = {
  Role,
  routes,
};
