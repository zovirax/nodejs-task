require('dotenv').config();

const { JWT_SECRET, JWT_EXPIRES_IN } = process.env;

module.exports = {
  secretKey: JWT_SECRET,
  expiresIn: JWT_EXPIRES_IN,
};
