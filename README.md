### Room4 Node.js Task

#### Overview
This application gets actual game statistics of your account in World of Tanks. 
The following steps describe how you can use it:

1) Sign up;
2) Authorize - you need to be authorized before making Wargaming API requests;
3) Make REST API calls on '/api/game/**' to use Wargaming API; 
 
#### Get started
npm install && npm run db:migrate && npm run start
  
#### Technologies
- Node.js/Express/ES6+
- REST API
- PostgreSQL
- Passport.js
- knex.js (SQL query builder and migrations)
- Wargaming API


#### Contributor
Yevtushenko Mikhail
