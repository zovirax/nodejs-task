const { assert } = require('../common/helpers/throw.helper');
const WorldOfTanksClient = require('../common/clients/WorldOfTanksClient');
const gameStatsQuery = require('../data/queries/game-stats.query');
const gameAccountQuery = require('../data/queries/game-account.query');
const userGameAccountQuery = require('../data/queries/user-game-account.query');
const { createError } = require('../common/errors/HttpError');
const { WARGAMING_ACCOUNT_NOT_FOUND, GAME_ACCOUNT_NOT_FOUND } = require('../common/errors/error-code.json');

const wotClient = new WorldOfTanksClient();

const addGameAccount = async ({ nickname, userId }) => {
  let gameAccountByNickname = await gameAccountQuery.findByNickname(nickname);

  if (!gameAccountByNickname) {
    const gameAccount = await wotClient.getAccountByNickname(nickname);
    assert(gameAccount, createError(WARGAMING_ACCOUNT_NOT_FOUND));
    const { accountId: id } = gameAccount;
    gameAccountByNickname = await gameAccountQuery.insert({ id, ...gameAccount });
  }

  const userGameAccount = await userGameAccountQuery
    .findByUserIdAndAccountId({ userId, accountId: gameAccountByNickname.id });

  if (!userGameAccount) {
    await userGameAccountQuery.insert({ userId, accountId: gameAccountByNickname.id });
  }
};

const getGameAccounts = userId => userGameAccountQuery.findByUserId(userId);

const getGameStats = async accountId => {
  const gameAccountById = await gameAccountQuery.findById(accountId);
  assert(gameAccountById, createError(GAME_ACCOUNT_NOT_FOUND));

  let statsByAccountId = await gameStatsQuery.findByAccountId({ accountId });
  if (!statsByAccountId) {
    const accountStatistics = await wotClient.getAccountStatistics(accountId);
    assert(accountStatistics, createError(WARGAMING_ACCOUNT_NOT_FOUND));
    statsByAccountId = await gameStatsQuery.insert({ accountId, ...accountStatistics });
  }
  return statsByAccountId;
};

const updateActualStats = async () => {
  const allAccountIds = await gameAccountQuery.findAllIds();

  const mapper = ({ id: accountId }) => wotClient.getAccountStatistics(accountId)
    .then(stats => gameStatsQuery.update({ accountId, ...stats }));

  await Promise.all(allAccountIds.map(mapper));
};

const deleteGameAccount = ({ userId, accountId }) => userGameAccountQuery
  .deleteByUserIdAndAccountId({ userId, accountId });

module.exports = {
  addGameAccount,
  deleteGameAccount,
  getGameAccounts,
  getGameStats,
  updateActualStats,
};
