const { insert } = require('../data/queries/user.query');
const { createJwt } = require('../common/helpers/jwt.helper');

const authenticate = userData => {
  const { id, email, fullName } = userData;
  return Promise.resolve({
    accessToken: createJwt({ sub: id }),
    user: { id, email, fullName },
  });
};

const register = async userData => {
  const user = await insert(userData);
  return authenticate(user);
};

module.exports = {
  register,
  authenticate,
};
