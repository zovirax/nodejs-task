const { Router } = require('express');
const gameService = require('../../services/game.service');
const {
  addGameAccountValidation,
  deleteGameAccountValidation,
  getAccountStatsValidation,
} = require('../middlewares/validation/game.validation');

const router = Router();

router.post('/accounts', addGameAccountValidation, (req, res, next) => {
  gameService.addGameAccount({ ...req.body, userId: req.user.id })
    .then(() => res.sendStatus(204))
    .catch(next);
});

router.delete('/accounts/:id', deleteGameAccountValidation, (req, res, next) => {
  gameService.deleteGameAccount({ userId: req.user.id, accountId: req.params.id })
    .then(() => res.sendStatus(204))
    .catch(next);
});

router.get('/accounts', (req, res, next) => {
  gameService.getGameAccounts(req.user.id)
    .then(accounts => res.ok({ accounts }))
    .catch(next);
});

router.get('/accounts/:id/stats', getAccountStatsValidation, (req, res, next) => {
  gameService.getGameStats(req.params.id)
    .then(stats => res.ok({ stats }))
    .catch(next);
});

module.exports = router;
