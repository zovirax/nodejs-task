const authRoutes = require('./auth.route');
const userRoutes = require('./user.route');
const gameRoutes = require('./game.route');

module.exports = app => {
  app.use('/api/auth', authRoutes);
  app.use('/api/user', userRoutes);
  app.use('/api/game', gameRoutes);
};
