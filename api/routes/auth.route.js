const { Router } = require('express');
const authService = require('../../services/auth.service');
const { loginValidation, registerValidation } = require('../middlewares/validation/auth.validation');
const registerMiddleware = require('../middlewares/register.middleware');
const loginMiddleware = require('../middlewares/login.middleware');
const { setAccessToken, clearAccessToken } = require('../../common/helpers/cookie.helper');

const router = Router();

const handleAuthResponse = response => authData => {
  const { accessToken, user } = authData || {};
  setAccessToken(response, accessToken).ok({ user, accessToken });
};

router.post('/register', registerValidation, registerMiddleware, (req, res, next) => {
  authService.register(req.user)
    .then(handleAuthResponse(res))
    .catch(next);
});

router.post('/login', loginValidation, loginMiddleware, (req, res, next) => {
  authService.authenticate(req.user)
    .then(handleAuthResponse(res))
    .catch(next);
});

router.post('/logout', (req, res) => {
  clearAccessToken(res).sendStatus(204);
});

router.get('/me', (req, res) => {
  const { id, email, fullName, role } = req.user;
  const user = { id, email, fullName, role };
  res.ok({ user: id ? user : undefined, authorized: !!id });
});

module.exports = router;
