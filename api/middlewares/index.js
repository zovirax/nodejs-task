const express = require('express');
const passport = require('passport');
require('../../config/passport/passport.config');
const cookieParser = require('cookie-parser');
const errorHandlerMiddleware = require('./error-handler.middleware');
const aclMiddleware = require('./acl.middleware');
const authorizationMiddleware = require('./authorization.middleware.js');
const responseMiddleware = require('./response.middleware');
const { createError } = require('../../common/errors/HttpError');
const errorCodes = require('../../common/errors/error-code.json');

module.exports = {
  preRoutes: app => {
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(passport.initialize({}));
    app.use(responseMiddleware);
    app.use('/api', [authorizationMiddleware, aclMiddleware]);
  },
  postRoutes: app => {
    app.use('*', (req, res, next) => {
      next(createError(errorCodes.NOT_FOUND));
    });
    app.use(errorHandlerMiddleware);
  },
};
