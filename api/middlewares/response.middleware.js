module.exports = (req, res, next) => {
  res.ok = data => res.send({ status: 'ok', data });
  next();
};
