const HttpError = require('../../common/errors/HttpError');
const { UNEXPECTED } = require('../../common/errors/error-code.json');

module.exports = (err, req, res, next) => {
  console.error(err);

  if (res.headersSent) {
    return next(err);
  }

  const { message, status, code } = err instanceof HttpError ? err : UNEXPECTED;

  return res.status(status)
    .send({
      status: 'error',
      error: {
        message,
        status,
        code,
      },
    });
};
