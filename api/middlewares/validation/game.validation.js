const { body } = require('express-validator');
const validator = require('./index');
const { idRules } = require('./shared.mixin');

const addGameAccountRules = [
  body('nickname')
    .isLength({
      min: 3,
    })
    .trim()
    .isString(),
];

const deleteGameAccountRules = [
  ...idRules,
];

const getAccountStatsRules = [
  ...idRules,
];

module.exports = {
  addGameAccountValidation: [addGameAccountRules, validator],
  deleteGameAccountValidation: [deleteGameAccountRules, validator],
  getAccountStatsValidation: [getAccountStatsRules, validator],
};
