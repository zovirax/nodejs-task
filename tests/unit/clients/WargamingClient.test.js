const chai = require('chai');
const { wotApplicationId: applicationId } = require('../../../config/wargaming.confg');
const { WargamingClient, WargamingApiMethod, WargamingBaseUrl } = require('../../../common/clients/WargamingClient');
chai.use(require('chai-as-promised'));

chai.should();

let wargamingClient;

beforeEach(() => {
  wargamingClient = new WargamingClient({ applicationId, baseURL: WargamingBaseUrl.WOT });
});

describe('WargamingClient - unit tests', () => {
  describe('#constructor', () => {
    it('should properly constructs itself', () => {
      const options = { applicationId, language: 'ru', timeout: 3000 };
      const testedClient = new WargamingClient(options);
      testedClient.should.have.property('language', options.language);
      testedClient.should.have.property('applicationId', options.applicationId);
    });
  });

  describe('request', () => {
    it('should throw an error, when `url` is not valid', async () => {
      await wargamingClient.request(WargamingApiMethod.GET, 'not-valid-url').should.to.eventually.rejected;
    });

    it('should throw an error, when `method` is not valid', async () => {
      await wargamingClient.request('not-valid-method', '/encyclopedia/tanks/').should.to.eventually.rejected;
    });

    it('should return data with status `ok`', () => {
      wargamingClient.request(WargamingApiMethod.GET, '/encyclopedia/tanks/')
        .then(data => {
          data.should.have.property('status', 'ok');
          data.should.have.property('data');
          data.should.have.property('meta');
        });
    });
  });
});
