const chai = require('chai');
const WorldOfTanksClient = require('../../../common/clients/WorldOfTanksClient');
chai.use(require('chai-as-promised'));

chai.should();

let wargamingClient;

beforeEach(() => {
  wargamingClient = new WorldOfTanksClient();
});

describe('WorldOfTanksClient - unit tests', () => {
  describe('getAccountByNickname', () => {
    it('should return correct data, when `nickname` is valid', async () => {
      const nickname = 'Point007';
      const expectedAccountId = 4564654;
      const account = await wargamingClient.getAccountByNickname(nickname);
      account.should.have.property('nickname', nickname);
      account.should.have.property('accountId', expectedAccountId);
    });

    it('should throw error, when `nickname` is not valid', async () => {
      const nickname = '42';
      await wargamingClient.getAccountByNickname(nickname).should.to.eventually.rejected;
    });
  });

  describe('getAccountStatistics', () => {
    it('should return correct data, when `accountId` is valid', async () => {
      const accountId = 4564654;
      const stats = await wargamingClient.getAccountStatistics(accountId);
      stats.should.have.property('battles');
      stats.should.have.property('survivedBattles');
      stats.should.have.property('wins');
      stats.should.have.property('losses');
      stats.should.have.property('xp');
    });
  });
});
