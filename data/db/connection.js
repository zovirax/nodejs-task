const knex = require('knex');
const knexConfig = require('../../knexfile');
const { env } = require('../../config/app.config');

module.exports = knex(knexConfig[env]);
