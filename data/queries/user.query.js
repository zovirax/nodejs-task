const knex = require('../db/connection');

const insert = ({ email, password, fullName }) => knex('user')
  .insert({ email, password, fullName })
  .returning('*')
  .then(rows => rows[0]);

const findByEmail = email => knex('user')
  .select('*')
  .where({ email })
  .first();

const findById = id => knex('user')
  .select('*')
  .where({ id })
  .first();

const changePassword = ({ password, id }) => knex('user')
  .update({ password })
  .where({ id })
  .returning('*');

const update = ({ id, fullName, email }) => knex('user')
  .update({ fullName, email })
  .where({ id })
  .returning('*')
  .then(rows => rows[0]);

module.exports = {
  insert,
  findByEmail,
  findById,
  changePassword,
  update,
};
