const knex = require('../db/connection');

const insert = ({ id, nickname }) => knex('game_account')
  .insert({ id, nickname })
  .returning('*')
  .then(rows => rows[0]);

const findById = id => knex('game_account')
  .select('*')
  .where({ id })
  .first();

const findByNickname = nickname => knex('game_account')
  .select('*')
  .whereRaw('lower(?) = lower(game_account.nickname)', [nickname])
  .first();

const findAllIds = () => knex('game_account')
  .select('id');

module.exports = {
  insert,
  findById,
  findByNickname,
  findAllIds,
};
