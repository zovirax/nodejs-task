const knex = require('../db/connection');

const insert = ({ userId, accountId }) => knex('user_game_account')
  .insert({ userId, accountId })
  .returning('*')
  .then(rows => rows[0]);

const deleteByUserIdAndAccountId = ({ userId, accountId }) => knex('user_game_account')
  .delete()
  .where({ userId, accountId });

const findByUserId = userId => knex('user_game_account')
  .select('accountId', 'nickname', 'createdAt')
  .where({ userId })
  .join('game_account', 'game_account.id', 'user_game_account.accountId');

const findByUserIdAndAccountId = ({ userId, accountId }) => knex('user_game_account')
  .select('*')
  .where({ userId, accountId })
  .first();

module.exports = {
  insert,
  deleteByUserIdAndAccountId,
  findByUserId,
  findByUserIdAndAccountId,
};
