const knex = require('../db/connection');

const insert = ({ accountId, battles, survivedBattles, wins, losses, xp }) => knex('game_statistics')
  .insert({ accountId, battles, survivedBattles, wins, losses, xp })
  .returning('*')
  .then(rows => rows[0]);

const update = ({ accountId, battles, survivedBattles, wins, losses, xp }) => knex('game_statistics')
  .update({ battles, survivedBattles, wins, losses, xp, updatedAt: new Date() })
  .where({ accountId })
  .returning('*');

const findByNickname = ({ nickname }) => knex('game_statistics')
  .select('*')
  .where({ nickname })
  .first();

const findByAccountId = ({ accountId }) => knex('game_statistics')
  .select('*')
  .where({ accountId })
  .first();

module.exports = {
  insert,
  update,
  findByNickname,
  findByAccountId,
};
